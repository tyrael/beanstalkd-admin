
## Introduction

A simple django made beanstalkd web admin tool. 

- Multi Beanstalkd Admin
- light admin pages
- easy to use and extension

## Preview
![](https://github.com/luke0922/beanstalkd-admin/blob/master/preview.png)

## Usage

### 1. install dependences

	$ pip install -r requirements.txt

### 2. add beanstalkd addresses

	$ vim settings.py
	
search `BEANSTALK_SERVERS` and add servers like following:

	BEANSTALK_SERVERS = [
      ['127.0.0.1', 11300],
      ['192.168.1.103', 11300]
    ]
 
### 3. init dbs

	$ python manage.py syncdb
	
You can add web system admin account in this way. 

### 4. run server
	
	$ python manage.py runserver
	
## Todo
...

## Licence

as you can see, this project forked from [django-jack](https://github.com/andreisavu/django-jack).

The licence follow django-jack project as apache 2.0. Please use this porject in the licence. thx.



